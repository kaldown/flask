from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
garage = Table('garage', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('name', VARCHAR(length=64)),
    Column('count', INTEGER),
    Column('date', DATE),
    Column('comment', VARCHAR(length=255)),
    Column('count_out', INTEGER),
    Column('date_out', DATE),
    Column('name_recipient', VARCHAR(length=255)),
    Column('place', VARCHAR(length=255)),
    Column('provider', VARCHAR(length=255)),
    Column('value', INTEGER),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['garage'].columns['value'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['garage'].columns['value'].create()
