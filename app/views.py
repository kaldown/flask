from app import app, db, models
from .models import Garage, User, Role, security, count_sum, not_empty,\
                    rollback_count
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_admin.form import rules
from wtforms.validators import required
from flask_security import current_user
from flask import url_for, redirect, request, abort
from flask_admin import helpers as admin_helpers
from wtforms.validators import ValidationError, InputRequired

class ModelView_for_Admin(ModelView):

    def is_accessible(self):
        if not current_user.is_active() or not current_user.is_authenticated():
            return False

        if current_user.has_role('administrator'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            if current_user.is_authenticated():
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))

class MyModelView(ModelView):

    column_display_pk = True
    action_disallowed_list = ['delete']

    def is_accessible(self):
        if not current_user.is_active() or not current_user.is_authenticated():
            self.can_create = False
            self.can_edit = False
            self.can_delete = False
            return False

        if current_user.has_role('administrator'):
            self.can_create = True
            self.can_edit = True
            self.can_delete = True
            return True

        if current_user.has_role('creator'):
            self.can_create = True
            self.can_edit = False
            self.can_delete = False
            return True

        return False

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            if current_user.is_authenticated():
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))

    form_create_rules = [
            rules.FieldSet(('date', 'name', 'count', 'value', 'provider',
                'place', 'date_out', 'count_out', 'name_recipient'), 'Create'),
            ]
    
    form_edit_rules = [
            rules.FieldSet(('date', 'name', 'count', 'value', 'provider',
                'place', 'date_out', 'count_out', 'name_recipient', 'comment'), 'Edit'),
            ]

    def on_model_change(self, form, model, is_created):
        if not_empty(form.name.data):

            if is_created:
                have_count = count_sum(form.count.name, form.name.data) -\
                            count_sum(form.count_out.name, form.name.data)
            else:
                id_row = self.get_pk_value(model)
                count, count_out = rollback_count(id_row)
                have_count = count_sum(form.count.name, form.name.data) -\
                            count_sum(form.count_out.name, form.name.data) -\
                            count + count_out
                            
            if form.count_out.data > have_count + form.count.data:
                raise ValidationError('На выход больше чем имеется')
        else:
            super().on_model_change(form, model, is_created)

    form_args = dict(
            #date = dict(validators=[required()]),
            name = dict(validators=[required()]),
            count = dict(validators=[InputRequired()]),
            #value = dict(validators=[required()]),
            #provider = dict(validators=[required()]),
            #place = dict(validators=[required()]),
            #date_out = dict(validators=[required()]),
            count_out = dict(validators=[InputRequired()]),
            #name_recipient = dict(validators=[required()]),
            #comment = dict(validators=[required()])
            )

    column_searchable_list = ['name', 'value', 'provider',
                'place', 'name_recipient', 'comment']

    column_filters = ('date', 'name', 'count', 'value', 'provider',
                'place', 'date_out', 'count_out', 'name_recipient', 'comment')

    column_sortable_list = ('date', 'name', 'count', 'value', 'provider',
                'place', 'date_out', 'count_out', 'name_recipient', 'comment')

    column_labels = dict(
            date='Дата поступления',
            count='Кол-во',
            name='Наименование',
            value='Ед. изм',
            provider='Поставщик',
            place='Объект',
            date_out='Дата выдачи',
            count_out='Кол-во выход',
            name_recipient='Ф.И.О. получившего',
            comment='Комментарий'
            )
    #create_modal = True #will be added after applied pull request from @pawl

admin = Admin(
        app,
        template_mode='bootstrap3',
        name='tdizel',
        base_template='my_master.html',
        url='/'
        )


admin.add_view(MyModelView(Garage, db.session, 'Склад'))
admin.add_view(ModelView_for_Admin(Role, db.session, 'Роли'))
admin.add_view(ModelView_for_Admin(User, db.session, 'Пользователи'))

@security.context_processor
def security_context_processor():
    return dict(
            admin_base_template=admin.base_template,
            admin_view=admin.index_view,
            h=admin_helpers
            )

