import os

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask_babelex import Babel

app = Flask(__name__)
app.config.from_object('config')

babel = Babel(app)

#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
db = SQLAlchemy(app)

from app import views, models
