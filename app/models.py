from datetime import datetime
from app import db, app
from flask.ext.security import Security, SQLAlchemyUserDatastore, \
                                UserMixin, RoleMixin, login_required

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.security import Security, SQLAlchemyUserDatastore,\
        UserMixin, RoleMixin, login_required

import psycopg2

class Garage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, default=datetime.now)
    name = db.Column(db.String(64), index=True)
    count = db.Column(db.Integer, default=0)
    value = db.Column(db.String(64))
    provider = db.Column(db.String(255))
    place = db.Column(db.String(255))
    date_out = db.Column(db.Date, default=datetime.now)
    count_out = db.Column(db.Integer, default=0)
    name_recipient = db.Column(db.String(255))
    comment = db.Column(db.String(255))

    def __repr__(self):
        return 'Name %r' % (self.name)

roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))
        

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))
        
    def __repr__(self):
        return self.name

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __repr__(self):
        return self.email

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

#conn = psycopg2.connect("dbname=tdizel user=pgsql")
from config import conn

def count_sum(what_to_sum, where_to_sum):
    with conn.cursor() as cur:
        cur.execute(
                """
                SELECT SUM(%s) FROM garage WHERE
                lower(name)=lower('%s')
                """
                % (what_to_sum, where_to_sum))
        return cur.fetchone()[0]

def not_empty(name_field):
    with conn.cursor() as cur:
        cur.execute(
                """
                SELECT name FROM garage WHERE lower(name)=lower('%s')
                """
                % (name_field))
        return cur.fetchone() is not None

def rollback_count(id_row):
    with conn.cursor() as cur:
        cur.execute(
                """
                SELECT count,count_out FROM garage WHERE id=%s
                """
                % (id_row))
        return cur.fetchone()
